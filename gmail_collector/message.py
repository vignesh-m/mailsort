import json
import sys
import base64

from bs4 import BeautifulSoup


class Message():

    def __init__(self):
        pass

    def __repr__(self):
        return (u"""
From: {}
To: {} 
CC: {}
Subject: {}
Body: 
{}
""".format(self.frm, self.to, self.cc, self.subject,
           self.body if len(self.body) < 10000 else self.body[:10000] + u" .... <Too Long>"))


class GMailMessage(Message):

    def __init__(self, gmail_message):
        self.response = gmail_message
        self.id = gmail_message['id']
        payload = gmail_message['payload']
        self.headers = payload['headers']
        self.payload = payload

        self.process_headers()
        self.process_payload()

    def process_payload(self):
        def get_text(part):
            if part['mimeType'] == 'text/html':
                body = part.get('body', None)
                if body and body['size'] > 0 and 'data' in body:
                    data = body['data']
                    data = base64.urlsafe_b64decode(data)
                    parsed = BeautifulSoup(data, 'lxml')
                    for script in parsed(["script", "style"]):
                        script.extract()
                    text = parsed.get_text()
                    lines = (line.strip() for line in text.splitlines())
                    text = u'\n'.join(line for line in lines if line)
                    return text
                return u""
            elif part['mimeType'] == 'text/plain':
                body = part.get('body', None)
                if body and body['size'] > 0 and 'data' in body:
                    data = body['data']
                    data = base64.urlsafe_b64decode(data)
                    # print('plain : {}'.format(data))
                    return data.decode('utf8')
                return u""
            elif 'parts' in part and len(part['parts']) > 0:
                s = u""
                for subpart in part['parts']:
                    s = get_text(subpart) + u"\n"
                return s
            return u""

        self.mime_type = self.payload['mimeType']
        self.body = get_text(self.payload)

    def process_headers(self):
        hvals = {}
        toget = ['Subject', 'To', 'CC', 'From', 'Date']
        for header in self.headers:
            if header['name'] in toget:
                hvals[header['name']] = header['value']
        self.subject = hvals.get('Subject', '')
        self.to = hvals.get('To', '')
        self.cc = hvals.get('CC', '')
        self.frm = hvals.get('From', '')


def main():
    msgs = json.load(open('mail_messages.json'))
    for msgr in msgs:
        msg = GMailMessage(msgr)
        print(msg)


if __name__ == '__main__':
    main()
