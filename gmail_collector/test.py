import argparse
import httplib2
import json

from apiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from oauth2client import tools
from oauth2client.file import Storage

SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'

parser = argparse.ArgumentParser(parents=[tools.argparser])
flags = parser.parse_args()


def get_credentials():
    store = Storage('creds_file')
    credentials = store.get()
    if credentials is None or credentials.invalid:
        flow = flow_from_clientsecrets('client_secret.json', scope=SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store, flags)
    return credentials


def main():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    gmail = build('gmail', 'v1', http=http)
    result = gmail.users().messages().list(userId='me', maxResults=10).execute()
    messages = []
    for elem in result['messages']:
        id = elem[u'id']
        message = gmail.users().messages().get(
            userId='me', id=id, format='full').execute()
        messages.append(message)
    # print(result)
    json.dump(messages, open('mail_messages.json', 'w'))


if __name__ == '__main__':
    main()
